# -*- coding: utf-8 -*-
import json
import re
import os,sys
from whoosh import scoring
from whoosh.index import open_dir
from whoosh.query import Or,Term,And
from whoosh.index import create_in
from whoosh.fields import Schema, TEXT, ID
from math import ceil

sys.path.insert(0,'../../')
from pyThaiwikiAns import (tokenizer,filemanagement)



def checlAlga(nameJson,corpus):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()
    count_all = 0
    ccosp = 0
    for i in range(len(dataJson["data"])):
        count_all +=1
        index = i
        qies = dataJson["data"][index]["question"]
        article_id = dataJson["data"][index]["article_id"]
        with open(corpus+"\\"+str(article_id)+".txt",'r',encoding="utf8") as fp:
            rawtext = fp.read()
            try:
                datacontent = re.match('<.*>(.*?)</.*>', rawtext) 
                segC = tokenizer.wikicuter(datacontent[1])
            except Exception as e:
                rawtext += "</doc>"
                datacontent = re.match('<.*>(.*?)</.*>', rawtext)
                segC = tokenizer.wikicuter(datacontent[1])
        segQ = tokenizer.wikicuter(qies)
        numQ = len(segQ)
        scoreQ = [x for x in segQ if x in segC]
        numscoreQ = len(scoreQ)
        ccosp += (numscoreQ/numQ)*100
        print(ccosp/(count_all))

def preDataSet(copus,nameDirSearch):
    schema = Schema(path=ID(stored=True),title=TEXT,content=TEXT)
    if not os.path.exists(nameDirSearch):
        os.mkdir(nameDirSearch)
    ix = create_in(nameDirSearch,schema)
    writer = ix.writer()
    filepaths = filemanagement.list_files(copus)
    for num,item in enumerate(filepaths):
        with open(corpus+"\\"+item,'r',encoding="utf8") as fp:
            IDpath = item.replace(".text","")
            print('prepare data search {}/{} = {}'.format(fileNumber, numFiles,fileNumber*100/numFiles))
            rawtext = fp.read()
        datacontent = re.match('<.*>(.*?)</.*>', rawtext) 
        try:
            len(datacontent[1])
        except Exception as e:
            rawtext += "</doc>"
            data_content = re.match('<.*>(.*?)</.*>', rawtext) 
        title_ceontent = re.findall('title="(.*?)"', rawtext) 
   		id_ceontent = re.findall('id="(.*?)"', rawtext) 

        print('prepare data search {}/{} = {}'.format(num, numFiles,num*100/numFiles)) 
        writer.add_document(path=u""+str(IDpath),title=,content=)
    writer.commit()


def checkSearch(nameJson,nameDirSearch):
    ix = open_dir(nameDirSearch)
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()
    dataErors = []     
    count_all = 0 
    count_no1 = 0
    count_no2 = 0 
    count_no3 = 0
    with  ix.searcher(weighting=scoring.BM25F(B=0.75, content_B=1.0, K1=1.5)) as searcher:
        for i in range(len(dataJson["data"])):
            count_all +=1
            index = i
            qies = dataJson["data"][index]["question"]
            article_id = dataJson["data"][index]["article_id"]
            wordseg = [x[0] for x in deepcut.tokenize(qies)]
            query = Or([Term("content", x) for x in wordseg if x != " "])
            results = searcher.search(query,limit=3)
            mainDocid = [x['nameDoc'] for x in results]
            try:
                manDoc1 = mainDocid[0] 
                manDoc2 = mainDocid[1]
                manDoc3 = mainDocid[2] 
                if(int(manDoc1) == int(article_id)):
                    count_no1 += 1
                elif( int(manDoc2) == int(article_id) ):
                    count_no2 += 1
                elif( int(manDoc3) == int(article_id) ):
                    count_no3 += 1
                else:
                    dataErors.append([str(manDoc1) + " == " + str(article_id) , mainDocid])
                print("TOP 1 :"+str(count_no1) + "/" + str(count_all) +" = "+ str( (count_no1/count_all)*100) + "%")
                print("TOP 2 :"+str(count_no2) + "/" + str(count_all) +" = "+ str( (count_no2/count_all)*100)  + "%" )
                print("TOP 3 :"+str(count_no3) + "/" + str(count_all) +" = "+ str( (count_no3/count_all)*100)  + "%")
                print("TOTAL :" +str(count_no3+count_no2+count_no1) + "/" + str(count_all) +" = "+ str(( (count_no3 + count_no1 +count_no2 )/count_all)*100)  + "%")
                print("------------------------------------------------------------------------")
            except Exception as e:
                pass
    return dataErors            


def SearchText(searchText,nameDirSearch,limits=3):
    ix = open_dir(nameDirSearch)
    with  ix.searcher(weighting=scoring.Frequency) as searcher:
        wordseg = [searchText]
        query = Or([Term("content", x) for x in wordseg])
        print(query)
        results = searcher.search(query,limit=limits)
        print(results)
        mainDocid = [x['nameDoc'] for x in results]
    return mainDocid
    

if __name__ == "__main__":
    

    nameJson = "dataRaw\\question-4000.json"
    corpus = "C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\wikinsc" #file text file
    nameDirSearch = "inderx1"

    #checlAlga(nameJson,corpus)
    #preDataSet(corpus,nameDirSearch)
    #errorDoc = checkSearch(nameJson,nameDirSearch)
    checkSearch(nameJson,nameDirSearch)
