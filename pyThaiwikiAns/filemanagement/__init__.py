# -*- coding: utf-8 -*-
import pickle
import os

def list_files(paths):
    return [f for f in os.listdir(paths) if f.endswith('.txt')]

def save_obj(obj, name ):
    with open('../../dataPre/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('../../dataPre/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

if __name__ == "__main__":
	#['1000.txt', '10000.txt', '100007.txt', '100008.txt', '10001.txt',...]
	print(list_files("../../dataRaw/documents-nsc/"))