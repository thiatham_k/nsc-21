# -*- coding: utf-8 -*-
import deepcut
from re import search
import sys
sys.path.insert(0,'../../')

def quesTokenize(questSegmen):
    quesType = ['ใช่หรือไม่ใช่', 'อะไร','เท่าไร','เท่าไหร่','เมื่อไหร่','ใช่หรือไม่', 'ใช่ไหม','เมื่อไร','ใช่มั้ย', 'หรือไม่','ที่ไหน','ที่ไหน','ใคร','ใด','ใด','กี่','ไหน','ไร']
    qiesTag = ""
    for g in quesType:
        if(search(g, questSegmen.strip())):
            qiesTag = g
            break
    spliteQes = questSegmen.split(qiesTag) 
    lefts = deepcut.tokenize(spliteQes[0]) if spliteQes[0] != ""  and len(spliteQes[0]) >= 2 else []
    rights = deepcut.tokenize(spliteQes[1]) if spliteQes[1] != "" and len(spliteQes[1]) >= 2 else []
    return lefts+[qiesTag]+rights

def contentTokenize(text,lsttext):
	return deepcut.tokenize(text,custom_dict=lsttext)

if __name__ == '__main__':
	#['พระวรวงศ์เธอ พระองค์เจ้าพีรพงศ์ภาณุเดช', ' ', 'สิ้นพระชนม์', 'ที่ไหน']
	print(quesTokenize("พระวรวงศ์เธอ พระองค์เจ้าพีรพงศ์ภาณุเดช สิ้นพระชนม์ที่ไหน"))

