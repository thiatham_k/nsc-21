# -*- coding: utf-8 -*-
import re
import json
import os,sys
import requests
from bs4 import BeautifulSoup
sys.path.insert(0,'../../')
import pyThaiwikiAns.tokenizer as tokenizer
import pyThaiwikiAns.filemanagement as filemanagement
import time


def questionPrepare():
    with open("../../dataRaw/question-17000.json","r",encoding="utf-8") as readJson:
        dataJson = json.loads(readJson.read())
    datas = []
    for allQ in range(len(dataJson["data"])):
        try:
            dataJson["data"][allQ]["question_tokenize"] = tokenizer.quesTokenize(dataJson["data"][allQ]["question"])
            datas.append(dataJson["data"][allQ])
        except Exception as erorr:
            print(erorr ," >> ", dataJson["data"][allQ]["question"])
    #"question_id":1,"question_type":1,
    #"question":"สุนัขตัวแรกรับบทเป็นเบนจี้ในภาพยนตร์เรื่อง Benji ที่ออกฉายในปี พ.ศ. 2517 มีชื่อว่าอะไร",
    #"answer":"ฮิกกิ้นส์",
    #"answer_begin_position":529,
    #"answer_end_position":538,
    #"article_id":115035
    filemanagement.save_obj(datas,"question-17000")


def getWordWiki(id):
    url = requests.get("https://th.wikipedia.org/wiki?curid="+str(id))
    htmltext = url.text
    listword = []
    soup = BeautifulSoup(htmltext)
    ops = soup.find('div',attrs={'id':'bodyContent'})
    data = ops.findAll('p')
    for div in data:
        links = div.findAll('a')
        for a in links:
            try:
                if("[" not in a["title"]):
                    listword.append(a["title"].replace("(ไม่มีหน้า)","").strip())
            except:
                pass
            try:
                if("[" not in a.text):
                      listword.append(a.text)
            except:
                pass
    return listword

def contentToJson():
    used = 0.0
    i = 0
    corpus = "../../dataRaw/documents-nsc/"
    listTxtFile = filemanagement.list_files(corpus)
    numFiles = len(listTxtFile)
    listTxtFile.sort()
    nows = 82483
    for num,item in enumerate(listTxtFile[nows:]):
        start = time.time()
        fp = open(corpus+"\\"+item,'r',encoding="utf8")
        rawtext = fp.read()
        fp.close()
        title_content = re.findall('title="(.*?)"', rawtext)
        id_ceontent = item.replace(".txt","")
        wordwiki = list(set( [x for x in getWordWiki(id_ceontent) if x != ""] ))
        print('สกัดไฟล์ {}/{}'.format(num+nows, numFiles))
        #print(wordwiki)
        title_seg = tokenizer.contentTokenize(title_content[0],wordwiki)
        with open('../../dataPre/jsonData/'+id_ceontent+".json", 'w') as outfile:
            json.dump({"id":id_ceontent,"title":title_seg,"wordwiki":wordwiki,"conent":tokenizer.contentTokenize(re.match('<.*>(.*?)</.*>', rawtext+"</doc>")[1],wordwiki),"title_search":title_seg + wordwiki},outfile)

        end = time.time()
        used += end - start
        i += 1
        print("avg : "+str(used / i) + "  " + str(((numFiles - (num+nows))  * used / i)/360))
        

if __name__ == "__main__":
    contentToJson()
