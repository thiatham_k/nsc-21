import re
from collections import Counter
from requests import get
from tltk import nlp, corpus
import gensim
import deepcut
import json
import time
import pandas as pd
from pythainlp.tokenize import word_tokenize
import matplotlib.pyplot as plt
import math
import numpy as np

text = ("สำนักเขตจตุจักรชี้แจงว่า ได้นำป้ายประกาศเตือนปลิงไปปักตามแหล่งน้ำ ในเขตอำเภอเมือง " "จังหวัดอ่างทอง หลังจากนายสุกิจ อายุ 65 ปี ถูกปลิงกัดแล้วไม่ได้ไปพบแพทย์")
nlp.chunk(text)


def textSegme(text):
    return deepcut.tokenize(text)
def getQes(QtokenizeTrue,top):
    try:
        return (QtokenizeTrue.index(top))
    except:
        return -100
read_file =  open("C:\\Users\\taa\\Desktop\\NSC2019\\skutman\\database\\QandA.json", encoding='utf-8')
dataJson = json.load(read_file)
read_file.close()  
allDoc = 0
mapAnspoint = 0
lenDatap = len(dataJson["data"])
sooss = ""
#for ci in range(lenDatap):
anspredic = ""
index = 0
count_num = 0
qies = dataJson["data"][index]["question"]
answer = dataJson["data"][index]["answer"]
article_id = dataJson["data"][index]["article_id"]

copus = "C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\documents-nsc"
path =  copus+"\\"+str(article_id)+".txt"
fp = open(path,'r',encoding="utf8") 
IDpath = path.replace(copus+"\\","").replace(".txt","")
rawtext = fp.read()
fp.close()
datacontent = re.match('<.*>(.*?)</.*>', rawtext) 
if(len(datacontent[1]) == 0):
    rawtext += "</doc>"
    datacontent = re.match('<.*>(.*?)</.*>', rawtext) 
contents = datacontent[1]
Ctokenize = textSegme(contents)
CtokenizeTrue = [x.strip() for x in Ctokenize if x not in ['',' ',""," "]]
CpostTing = [x[1] for x in nlp.pos_tag_wordlist(CtokenizeTrue)] 
tag_Cpos = [[CtokenizeTrue[x],CpostTing[x]] for x in range(len(CpostTing))]

questions = qies
Qtokenize = textSegme(questions)
QtokenizeTrue = [x.strip() for x in Qtokenize if x not in ['',' ',""," "]]
QpostTing = [x[1] for x in nlp.pos_tag_wordlist(QtokenizeTrue)] 

joinlist = []
for x in QtokenizeTrue:
    joinlist += [i for i,g in enumerate(CtokenizeTrue) if g == x]
try:
    ibnde =  [CtokenizeTrue.index(x.strip()) for x in textSegme(answer) if x not in ['',' ',""," "]]
except:
    ibnde = [i for i,x in enumerate(CtokenizeTrue) if x in answer]



joinlist += ibnde
joinlist = list(set(joinlist))
joinlist.sort()

cc1 = [[CtokenizeTrue[i],getQes(QtokenizeTrue,CtokenizeTrue[i]),CpostTing[i]] for i in joinlist]
cc2 = ([ [getQes(QtokenizeTrue,CtokenizeTrue[i]),i] for i in joinlist])

pastse = 0
datp = 0
datasss = []
for x in range(len(cc2)):
    if(cc2[x][0] > pastse):
        datp += 1
    else:
        datasss.append(datp)
        datp = 1
    pastse = cc2[x][0]
datasss.append(datp)
print(datasss)
passV = 0
mass = []
for x in datasss:
    cc2[passV+1:passV+x]
    mass.append(cc2[passV:passV+x])
    passV += x
print(mass)
numAl = [i[1] for i in sorted(mass, key=len)[-1]]
maxd = max(numAl)+1
mind = min(numAl)+1
soe = [x for x in range(mind,maxd+10)]
numAl += soe

for x in range(len(tag_Cpos)):
    if (tag_Cpos[x][1] == "CCONJ"  or (tag_Cpos[x][0] not in  QtokenizeTrue and CtokenizeTrue[x] not in [x.strip() for x in textSegme(answer) if x not in ['',' ',""," "]])):
        tag_Cpos[x][0] = 0
print("----------------------------------------------------------------------------------------------------------")
print("Questi : " + questions)
print("RElation : " + " ".join([CtokenizeTrue[x] for x in  numAl]))
print("Trueans : " + answer)
#print("Predict : " + " ".join([CtokenizeTrue[x] for x in ibnde]))
print("----------------------------------------------------------------------------------------------------------")
dataTable = pd.DataFrame(cc1, columns=['Text','flow','vol'])
#dataTable = pd.DataFrame(cc2, columns=['flow','no'])
#display(dataTable)
dataTable = dataTable.cumsum()
plt.figure()
dataTable.plot()
plt.title('Question id 0')
plt.xlabel('i') 
plt.ylabel('relation Ques and Doc')
 