import json
import pickle
from os.path import isfile, join
import random
import math
import operator
import re
'''
def wordListToFreqDict(wordlist):
    wordfreq = [wordlist.count(p) for p in wordlist]
    return dict(zip(wordlist,wordfreq))

if __name__ == '__main__':
	with open("dataPre/what.json","r",encoding="utf-8") as writs:
		data = json.load(writs)
	cc = ['type_q', 'id_q', 'id_c', 'question', 'answer', 'tokenize_q', 'tokenize_c', 'tokenize_qRc', 'relate_10_10', 'relate_20_20', 'relate_30_30', 'relare_40_40', 'relare_50_50', 'relate_10_10_qRc', 'relate_20_20_qRc', 'relate_30_30_qRc', 'relare_40_40_qRc', 'relare_50_50_qRc']
	#random.shuffle(data)
	numdataTrain = .95
	train_data = data[:int((len(data)+1)*numdataTrain)] #Remaining 80% to training set
	test_data = data[int(len(data)*numdataTrain+1):] #Splits 20% data to test set
	#list data
	listwordone = []
	for x in train_data:
		relate_10_10 = x["relate_10_10"]
		tokenize_q = x["tokenize_q"]
		question = x["question"]
		answer = x["answer"]
		print(relate_10_10)
		#1.check in answer 
		method1 = list(set([x for x in tokenize_q if relate_10_10[10].find(x) != -1]))
		try:
			indexQ = tokenize_q.index("อะไร")
			if(len(method1) == 1 and method1[0] != " "):
				listwordone.append(tokenize_q[ tokenize_q.index(method1[0]) : indexQ + 1])
			else:
				q_s_rever = tokenize_q[:indexQ]
				q_s_rever.reverse()
				datain = [g for g in q_s_rever if g in relate_10_10 and g != " "]
				indexofs = [relate_10_10.index(c) for c in datain if  11 >= relate_10_10.index(c) ]
				indexofs.sort()
				indexofs.reverse()
				if(indexofs != []):
					for ac in indexofs[:3]:
						try:
							listwordone.append(tokenize_q [ tokenize_q.index(ac) : indexQ + 1] )
						except Exception as e:
							pass
							
				else:
					#print(tokenize_q ," >> ",relate_10_10)
					pass
		except Exception as e:
			pass
	listwordone = [x for x in listwordone if x != []]
	for c in range(len(listwordone)):
		listwordone[c][0] = "<k>"+listwordone[c][0] + "</k>"
		listwordone[c][-1] = "<q>"+listwordone[c][-1] + "</q>"
'''
def wordListToFreqDict(wordlist):
    wordfreq = [wordlist.count(p) for p in wordlist]
    return dict(zip(wordlist,wordfreq))

def save_obj(obj, name ):
    with open('dataPre/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('dataPre/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def createFreqen(nameFile):
	with open(nameFile,"r",encoding="utf-8") as writs:
		data = json.load(writs)
	cc = ['type_q', 'id_q', 'id_c', 'question', 'answer', 'tokenize_q', 'tokenize_c', 'tokenize_qRc', 'relate_10_10', 'relate_20_20', 'relate_30_30', 'relare_40_40', 'relare_50_50', 'relate_10_10_qRc', 'relate_20_20_qRc', 'relate_30_30_qRc', 'relare_40_40_qRc', 'relare_50_50_qRc']
	#random.shuffle(data)
	numdataTrain = .95
	train_data = data[:int((len(data)+1)*numdataTrain)] #Remaining 80% to training set
	test_data = data[int(len(data)*numdataTrain+1):] #Splits 20% data to test set
	#list data
	dicSave = {}
	for x in train_data:
		relate_10_10 = x["relate_10_10"]
		tokenize_q = x["tokenize_q"]
		question = x["question"]
		answer = x["answer"]
		try:
			lenans = relate_10_10[10]
			relate_10_10.pop(10)
			relate_10_10 = list(set([x for x in relate_10_10 if x != " " and x != 0] + [g for g in tokenize_q if lenans.find(g) != -1]))
			wordFreq = wordListToFreqDict(x["tokenize_qRc"])
			for gg in relate_10_10:
				print(gg)
				if lenans.find(gg) != -1:
					weightnum = 100
				else:
					weightnum = 1
				if gg in dicSave:
					dicSave[gg]["len_con"] += 1*weightnum
					try:
						dicSave[gg]["len_in_con"] += (wordFreq[gg] / len(x["tokenize_qRc"]))
					except KeyError:
						dicSave[gg]["len_in_con"] += 1
					dicSave[gg]["qID"].append(x["id_q"])
					dicSave[gg]["ansText"].append(answer)
				else:
					try:
						dicSave[gg] = {"len_con":1*weightnum,"len_in_con":wordFreq[gg],"qID":[x["id_q"]],"ansText":[answer] }
					except KeyError:
						dicSave[gg] = {"len_con":1*weightnum,"len_in_con":1,"qID":[x["id_q"]],"ansText":[answer] }
		except Exception as e:
			print(e)

	save_obj(dicSave,"freqKey")
	save_obj(test_data,"trainData")

def takeClosest(num,collection):
   return min(collection,key=lambda x:abs(x-num))

def keywordExtracbyFreq(nameFile):
	listKey = []
	freqKey = load_obj("freqKey")
	trainData =  load_obj("trainData")
	'''
	for x in freqKey:
		listKey.append(x.strip())
	'''
	listKey = load_obj("FreqScore")
	with open(nameFile,"r",encoding="utf-8") as writs:
		data = json.load(writs)
	value_in_data = ['type_q', 'id_q', 'id_c', 'question', 'answer', 'tokenize_q', 'tokenize_c', 'tokenize_qRc', 'relate_10_10', 'relate_20_20', 'relate_30_30', 'relare_40_40', 'relare_50_50', 'relate_10_10_qRc', 'relate_20_20_qRc', 'relate_30_30_qRc', 'relare_40_40_qRc', 'relare_50_50_qRc']
	#random.shuffle(data)
	numdataTrain = .90
	train_data = data[:int((len(data)+1)*numdataTrain)] #Remaining 80% to training set
	test_data = data[int(len(data)*numdataTrain+1):] #Splits 20% data to test set
	#list data
	for x in train_data:
		relate_10_10 = x["relate_10_10"]
		tokenize_q = x["tokenize_q"]
		question = x["question"]
		answer = x["answer"]
		Qestype = [g for g in tokenize_q if g in ['ใคร', 'อะไร', 'ที่ไหน','ไหน', 'เมื่อไร','ใด', 'กี่', 'เท่าไร' ,'ใช่หรือไม่', 'ใช่ไหม', 'ใช่มั้ย', 'ใช่หรือไม่ใช่', 'หรือไม่']]
		try:
			indexQ = tokenize_q.index(Qestype[0])
			for y in tokenize_q:
				if y in listKey:
					indexY = takeClosest(indexQ,[n for n, i in enumerate(tokenize_q) if i == y])
					cutList = []
					if(indexY != indexQ + 1):
						if(indexQ + 1 > indexY):
							cutList = tokenize_q[ indexY : indexQ + 1]
							cutList[0] = "<k>"+cutList[0] + "</k>"
							cutList[-1] = "<q>"+cutList[-1] + "</q>"
						else:
							cutList = tokenize_q[indexQ: indexY]
							cutList[-1] = "<k>"+cutList[-1] + "</k>"
							cutList[0] = "<q>"+cutList[0] + "</q>"
						if( 10 >= len(cutList) > 1):
							print("|".join(cutList))
		except Exception as e:
			print(e)


def scoreKeyWord():
	ro = 0
	sod = 0
	freqKey = load_obj("freqKey")
	trainData =  load_obj("trainData")
	for c in trainData:
		relate_10_10 = c["relate_10_10"]
		tokenize_q = c["tokenize_q"]
		question = c["question"]
		answer = c["answer"]
		resr = []
		sod +=1
		for l in [k for k in relate_10_10 if k != 0 and k != " "]:
			if(l in freqKey):
				resr.append(l)
		if (resr == []):
			ro += 1
	print(((sod-ro)/sod)*100)

def FreqScore():
	listKey = {}
	freqKey = load_obj("freqKey")
	trainData =  load_obj("trainData")
	for x in freqKey:
		cso =  math.log(freqKey[x]["len_con"]/freqKey[x]["len_in_con"])
		if(freqKey[x]["len_con"] > 5):
			listKey[x] = cso
	sorted_x = sorted(listKey.items(), key=operator.itemgetter(1))
	regex = re.compile('''[@_!#$%^&*()<>?/\|}{~:"', “”;°-]''') 
	for g in sorted_x:
	    if(regex.search(g[0]) != None or g[0].isdigit() or g[0].isalpha() or g[0] == '' or g[0] ==  '[' or g[0] == ']' or g[0] in ['ใคร', 'อะไร', 'ที่ไหน','ไหน', 'เมื่อไร','ใด', 'กี่', 'เท่าไร' ,'ใช่หรือไม่', 'ใช่ไหม', 'ใช่มั้ย', 'ใช่หรือไม่ใช่', 'หรือไม่']): 
	        del listKey[g[0]]
	seglist = list(listKey.keys())
	seglist.sort()
	save_obj(seglist,"FreqScore")




if __name__ == "__main__":
	nameFile = "dataPre\\mainDataSet.json"
	#createFreqen(nameFile)
	#FreqScore()
	scoreKeyWord()
