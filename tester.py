# -*- coding: utf-8 -*-

import keywordGeneration
import json
import re
import os
import deepcut
import time
from pythainlp.tokenize import word_tokenize
import autoGenKey


def scoreTest(nameJson,nameDirSearch,wordQes="อะไร"):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()  
    dataVacabs = keywordGeneration.load_obj("keywordQ")
    DATA_KEY = keywordGeneration.load_obj("keywordGenerate")
    allDoc = 0
    mapAnspoint = 0
    lenDatap = len(dataJson["data"])
    for i in range(lenDatap):
        anspredic = ""
        index = i
        count_num = 0
        qies = dataJson["data"][index]["question"]
        answer = dataJson["data"][index]["answer"].replace(" ","")
        article_id = dataJson["data"][index]["article_id"]
        texts = qies
        if (qies.find(wordQes) != -1):
            try:
                dataWord,keywordgen = keywordGeneration.keyData(dataVacabs,DATA_KEY,texts)
                with open("C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\documents-nsc"+"\\"+str(article_id)+".txt","r",encoding="utf-8") as readD:
                    textread = readD.read() 
                doc = re.findall("<.*>(.*?)</.*>",textread)[0]
                cententSeg =  [x for x in deepcut.tokenize(doc,custom_dict=dataWord) if x != " " or x != ""]
                for c in keywordgen:
                    indices = [i for i, x in enumerate(cententSeg) if x == c[1]] + [cententSeg.index(g) for g in cententSeg if g.find(c[1]) != -1]
                    mapAns = False
                    indices = list(set(indices))
                    textjoin = ""
                    for x in indices:
                        count_num += len(indices)
                        textjoin = "".join(cententSeg[x-10:x+10]).replace(" ","")
                        print(textjoin)
                        if (textjoin.find(answer) != -1):
                            mapAns = True
                            anspredic = textjoin
                            break
                    if(mapAns) :
                        mapAnspoint += 1
                        break
                allDoc += 1
                print("-------------------------------------------------------") 
                print(' STA: {} \n File: {}/{} \n Qes: {} \n Ans: {} \n Pre: {} \n key: {} \n range: {} \n Map: {}/{} = {}'.format(str(mapAns),allDoc, lenDatap,qies,answer,anspredic,keywordgen,count_num,mapAnspoint,allDoc,mapAnspoint/allDoc*100))
                print("-------------------------------------------------------") 
            except KeyboardInterrupt:
                raise
            except:
                pass

def scoreTestV2(nameJson,nameDirSearch,size=5,wordQes="อะไร"):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()  
    dataVacabs = keywordGeneration.load_obj("keywordQ")
    DATA_KEY = keywordGeneration.load_obj("keywordGenerate")
    allDoc = 0
    mapAnspoint = 0
    lenDatap = len(dataJson["data"])
    for i in range(lenDatap):
        anspredic = ""
        index = i
        count_num = 0
        qies = dataJson["data"][index]["question"]
        answer = dataJson["data"][index]["answer"].replace(" ","")
        article_id = dataJson["data"][index]["article_id"]
        test_for_append = []
        #if (qies.find(wordQes) != -1):
        dataWord,keywordgen = keywordGeneration.keyData(dataVacabs,DATA_KEY,qies)
        if(keywordgen != []):
            with open("C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\documents-nsc"+"\\"+str(article_id)+".txt","r",encoding="utf-8") as readD:
                textread = readD.read() 
            doc = re.findall("<.*>(.*?)</.*>",textread)[0]
            cententSeg =  deepcut.tokenize(doc,custom_dict=dataWord)
            preContent_Seg = cententSeg.copy()
            str_keyword = [c[1] for c in keywordgen]
            index_keword = [c[2] for c in keywordgen]
            for x, vals in enumerate(preContent_Seg):
                if vals == " "  or  (vals not in str_keyword and len([ar for ar in str_keyword if ar.find(vals) == -1]) > 0):
                    preContent_Seg[x] = 0
                else:
                    presc = [str_keyword.index(r) for r in str_keyword if r == vals or r.find(vals) != -1]
                    preContent_Seg[x] = index_keword[presc[0]]

            for idx, val in enumerate(preContent_Seg):
                if(val != 0):
                    test_for_append.append([len(set( preContent_Seg[idx-size:idx+size] )),idx])

            test_for_append.sort()
            try:
                maxvs = test_for_append[-1][0]
                indices = [v[1] for v in test_for_append if v[0] == maxvs]
                mapAns = False
                indices = list(set(indices))
                textjoin = ""

                for y in indices:
                    count_num += len(indices)
                    textjoin = "".join(cententSeg[y-10:y+10]).replace(" ","")
                    print(textjoin)
                    if (textjoin.find(answer) != -1):
                        mapAns = True
                        anspredic = textjoin
                        break
                if(mapAns) :
                    mapAnspoint += 1
                allDoc += 1
                print("-------------------------------------------------------") 
                print(' STA: {} \n File: {}/{} \n Qes: {} \n Ans: {} \n Pre: {} \n key: {} \n range: {} \n Map: {}/{} = {}'.format(str(mapAns),allDoc, lenDatap,qies,answer,anspredic,keywordgen,count_num,mapAnspoint,allDoc,mapAnspoint/allDoc*100))
                print("-------------------------------------------------------") 
            except:
                allDoc += 1

def cuteringt(questSegmen):
    quesType = ['ใคร', 'อะไร', 'ที่ไหน','ไหน', 'เมื่อไร','ใด', 'กี่', 'เท่าไร','ไร','ใช่หรือไม่', 'ใช่ไหม', 'ใช่มั้ย', 'ใช่หรือไม่ใช่', 'หรือไม่']
    for g in quesType:
        if(questSegmen.find(g) != -1):
            qiesTag = g
            break
    spliteQes = questSegmen.split(qiesTag) 
    lefts = deepcut.tokenize(spliteQes[0]) if spliteQes[0] != ""  and len(spliteQes[0]) >= 2 else []
    rights = deepcut.tokenize(spliteQes[1]) if spliteQes[1] != "" and len(spliteQes[1]) >= 2 else []
    return lefts+[qiesTag]+rights


def spliteQes(nameJson,nameDirSearch):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()  
    lenDatap = len(dataJson["data"])
    saveDataRAw = []
    for i in range(lenDatap):
        index = i
        qies = dataJson["data"][index]["question"]
        answer = dataJson["data"][index]["answer"]
        question_id = dataJson["data"][index]["question_id"]
        article_id = dataJson["data"][index]["article_id"]
        #contentSegmen = deepcut.tokenize(textread,custom_dict=questSegmen+[answer])

        with open("C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\wikinsc"+"\\"+str(article_id)+".json","r",encoding="utf-8") as readD:
            textread = json.load(readD)
        try:
            questSegmen = cuteringt(qies)
            contentSegmen = textread
            for xx in range(len(contentSegmen)):
                if(contentSegmen[xx] == ">" or contentSegmen[xx].find(">") != -1 ):
                    if(contentSegmen[xx] == ">" or contentSegmen[xx] =='">'):
                        contentSegmen = contentSegmen[xx+1:]
                        break
                    else:
                        contentSegmen = contentSegmen[xx:]
                        contentSegmen[0] = contentSegmen[0].replace(">","").replace('">','')
                        break
            if("</doc>\n" in contentSegmen[-1]):
                contentSegmen[-1] = contentSegmen[-1].replace("</doc>\n","")
            domday = contentSegmen.copy()
            for p in range(len(domday)):
                if answer.find(domday[p]) != -1 or answer in domday[p] or  answer == domday[p]:
                    domday[p] = 1
                else:
                    domday[p] = 0
            passed = 0
            max_passed = 0
            count_d = 0
            list_check = []
            for cc in range(1,len(domday)):
                if(domday[cc] == domday[cc-1] == 1):
                    count_d += 1
                if(domday[cc] == 1 and domday[cc-1] == 0):
                    count_d = 1
                if( (domday[cc] == 0 and domday[cc-1] == 1) or cc == len(domday)-1 ):
                    list_check.append([count_d,cc-count_d])
                    count_d = 0
            list_check.sort()
            maxlongans = list_check[-1][0]
            allset = [a for a in list_check if a[0] == maxlongans]
            for lo in allset:
                getAns = "".join(contentSegmen[lo[1]:lo[1]+maxlongans]).strip()
                if(answer.strip() == getAns):
                    contentSegmen[lo[1]] = getAns
                    for x in range(1,maxlongans):
                        try:
                            contentSegmen.pop(lo[1]+x) 
                        except:
                            break
            wordQes = [g for g in questSegmen]
            allnumQ = [i for i, x in enumerate(contentSegmen) if x == answer]
            dataList = contentSegmen.copy()
            dataSegs = contentSegmen.copy()
            for x in range(len(dataList)):
                if dataList[x] not in questSegmen and dataList[x]!= answer :
                    dataList[x] = 0

            for numQuestion in allnumQ:
                saveDataRAw.append({
                        "type_q": wordQes[0]
                        ,"id_q": question_id
                        ,"id_c": article_id
                        ,"question":qies
                        ,"answer": answer
                        ,"tokenize_q":questSegmen 
                        ,"tokenize_c":contentSegmen
                        ,"tokenize_qRc":dataList 
                        ,"relate_10_10":dataSegs[numQuestion-10:numQuestion+10]
                        ,"relate_20_20":dataSegs[numQuestion-20:numQuestion+20]
                        ,"relate_30_30":dataSegs[numQuestion-30:numQuestion+30]
                        ,"relare_40_40":dataSegs[numQuestion-40:numQuestion+40]
                        ,"relare_50_50":dataSegs[numQuestion-50:numQuestion+50]
                        ,"relate_10_10_qRc":dataList[numQuestion-10:numQuestion+10]
                        ,"relate_20_20_qRc":dataList[numQuestion-20:numQuestion+20]
                        ,"relate_30_30_qRc":dataList[numQuestion-30:numQuestion+30]
                        ,"relare_40_40_qRc":dataList[numQuestion-40:numQuestion+40]
                        ,"relare_50_50_qRc":dataList[numQuestion-50:numQuestion+50]
                        })
            print(questSegmen)
        except Exception as e:
            print(str(e) + " " + qies)


    with open("cccx.json","w") as fast:
        fast.write(json.dumps(saveDataRAw))


def relateC_Q(nameJson,nameDirSearch):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()  
    dataVacabs = keywordGeneration.load_obj("keywordQ")
    DATA_KEY = keywordGeneration.load_obj("keywordGenerate")
    allDoc = 0
    mapAnspoint = 0
    lenDatap = len(dataJson["data"])
    for i in range(lenDatap):
        anspredic = ""
        index = i
        count_num = 0
        qies = dataJson["data"][index]["question"]
        answer = dataJson["data"][index]["answer"]
        article_id = dataJson["data"][index]["article_id"]
        with open("C:\\Users\\taa\\Desktop\\ตัดคำ\\thai-word-segmentation\\documents-nsc"+"\\"+str(article_id)+".txt","r",encoding="utf-8") as readD:
                textread = readD.read()
        questSegmen = deepcut.tokenize(qies,custom_dict=['ใคร', 'อะไร', 'ที่ไหน','ไหน', 'เมื่อไร','ใด', 'กี่', 'เท่าไร' ,'ใช่หรือไม่', 'ใช่ไหม', 'ใช่มั้ย', 'ใช่หรือไม่ใช่', 'หรือไม่'])
        contentSegmen = deepcut.tokenize(textread,custom_dict=[questSegmen])
        try:
            numQuestion = contentSegmen.index(answer)
            dataList = contentSegmen[numQuestion-20:numQuestion+10] 
            for x in range(len(dataList)):
                if  dataList[x] == answer:
                     dataList[x] = 123
                if dataList[x] not in questSegmen and dataList[x]!= 123 :
                    dataList[x] = 0
            print("-------------------------------------------------------------")
            print(qies)
            print(answer)
            print(dataList)
        except:
            pass
                


def Tester(nameJson,nameDirSearch):
    read_file =  open(nameJson, encoding='utf-8')
    dataJson = json.load(read_file)
    read_file.close()  
    dataVacabs = keywordGeneration.load_obj("keywordQ")
    DATA_KEY = keywordGeneration.load_obj("keywordGenerate")
    sooss = ""
    freQues = []
    lenDatap = len(dataJson["data"])
    for i in range(lenDatap):
        index = i
        qies = dataJson["data"][index]["question"]
        texts = qies
        try:
            topdata,keywordgen = keywordGeneration.keyData(dataVacabs,DATA_KEY,texts)
            print(keywordgen)
            sooss += qies  +" >> " + " ".join([x[1] for x in keywordgen])  + "\n"
            freQues += [x[1] for x in keywordgen]
        except KeyboardInterrupt:
            raise    
        except:
            sooss += qies  +" >>  erorr can't extract \n"
    with open("reportKeyGen.txt","w",encoding="utf-8") as ww:
         ww.write(sooss)
    print(autoGenKey.wordListToFreqDict(freQues))
                        
if __name__ == "__main__":
    nameJson = "dataRaw\\question-4000.json"
    scoreTest(nameJson,"indexdir")
    #cuteringt('กรมทรัพยากรน้ำบาดาลในประเทศไทยสังกัดกระทรวงใด')