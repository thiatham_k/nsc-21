import pickle
from os.path import isfile, join
import re
from collections import OrderedDict
import deepcut
from pythainlp.tokenize import word_tokenize
from pythainlp.tag import pos_tag



def save_obj(obj, name ):
    with open('dataPre/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('dataPre/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
    
def revertInt(dataInv):
    return int(str(dataInv)[::-1])

def createVocabulary(pathData,namePkl,loadData=True): #make diction vocab data
    if(loadData):
        DATA_DIGTEST = load_obj(namePkl)
    else:
        DATA_DIGTEST = []
        
    input_file = open(pathData,"r",encoding="utf-8")
    for line in input_file.readlines():
        for token in line.split('|'):
          stringIden = re.findall("<.*>(.*?)</.*>", token)
          if(stringIden != []):
             DATA_DIGTEST.append(stringIden[0]) 
          else:
             DATA_DIGTEST.append(token)
    input_file.close()
            
            
    dataDic = OrderedDict(map(tuple, map(reversed, enumerate(set(DATA_DIGTEST)))))
    save_obj(dataDic,namePkl)

def arrayNumSearch(listword):
    numSearch = {}
    count = 0
    for x in listword:
        if( "<k>" in x ):
            numSearch["k"] = count
        if( "<q>" in  x ):
            numSearch["q"] = count
        count += 1
    return numSearch
            
def get_input_labels(dataVacab,listWord):
  datas = []
  for listTag in listWord:
      datas.append(dataVacab.get(listTag,0))
  return datas

def get_output_labels(dataVacab,listWord):
  datas = []
  datakeys = list(dataVacab.keys())
  for ints in listWord:
      datas.append(datakeys[ints])
  return datas
    
def prepareData(namePkl,pathData,dataVacab,loadData=True):
    if(loadData):
        DATA_KEY = load_obj(namePkl)
    else:
        DATA_KEY = {}
    errorQ = []
    wordQuestion= []
    wordKeyword = []
    errorSentene = []
    input_file = open(pathData,"r",encoding="utf-8")
    for line in input_file.readlines():
        listSplite = line.split('|')
        QandK = arrayNumSearch(listSplite)
        if(len(QandK) == 2):
            listSplite[QandK["q"]] = re.findall("<.*>(.*?)</.*>", listSplite[QandK["q"]] )[0]
            print(line)
            listSplite[QandK["k"]] = re.findall("<.*>(.*?)</.*>", listSplite[QandK["k"]] )[0]
            wordQuestion.append(listSplite[QandK["q"]])
            wordKeyword.append(listSplite[QandK["k"]])
            if(QandK["q"] - QandK["k"] > 0):
                listMain = listSplite[QandK["k"]+1:QandK["q"]+1]
                listLabel = get_input_labels(dataVacab,listMain)
                mainQ = dataVacab.get(listSplite[QandK["q"]],0)
                if len(listLabel) > 1:
                    joinarray = "".join(map(str,listLabel)) 
                else :
                    joinarray = str(listLabel[0])
                    
                if mainQ in DATA_KEY :
                    DATA_KEY[mainQ]["left"] += 1 
                    DATA_KEY[mainQ]["data"].append( int("2"+joinarray) )
                    DATA_KEY[mainQ]["data"] = list(set(DATA_KEY[mainQ]["data"]))
                else:
                    DATA_KEY[mainQ] = {"left":0,"right":0,"data":[]}
                    DATA_KEY[mainQ]["left"] = 1
                    DATA_KEY[mainQ]["right"] = 0
                    DATA_KEY[mainQ]["data"] = [int("2"+joinarray)]
                    
            if(QandK["q"] - QandK["k"] < 0):
                listMain = listSplite[QandK["q"]:QandK["k"]]
                listLabel = get_input_labels(dataVacab,listMain)
                mainQ = dataVacab.get(listSplite[QandK["q"]],0)
                
                if len(listLabel) > 1:
                    joinarray = "".join(map(str,listLabel)) 
                else :
                    joinarray = str(listLabel[0])
                    
                if mainQ in DATA_KEY :
                    DATA_KEY[mainQ]["right"] += 1 
                    DATA_KEY[mainQ]["data"].append(int("1"+joinarray))
                    DATA_KEY[mainQ]["data"] = list(set(DATA_KEY[mainQ]["data"]))
                else:
                    DATA_KEY[mainQ] = {"left":0,"right":0,"data":[]}
                    DATA_KEY[mainQ]["left"] = 0
                    DATA_KEY[mainQ]["right"] = 1
                    DATA_KEY[mainQ]["data"] = [int("1"+joinarray)]
            else:   
                errorSentene.append(line)
        else:
            errorQ.append(line)
    save_obj(DATA_KEY, namePkl )
    return wordQuestion,wordKeyword,DATA_KEY,errorQ,errorSentene


def cuteringt(questSegmen):
    quesType = ['ใคร', 'อะไร', 'ที่ไหน','ไหน', 'เมื่อไร','ใด', 'กี่', 'เท่าไร','ไร','ใช่หรือไม่', 'ใช่ไหม', 'ใช่มั้ย', 'ใช่หรือไม่ใช่', 'หรือไม่']
    for g in quesType:
        if(questSegmen.find(g) != -1):
            qiesTag = g
            break
    spliteQes = questSegmen.split(qiesTag) 
    lefts = deepcut.tokenize(spliteQes[0]) if spliteQes[0] != ""  and len(spliteQes[0]) >= 2 else []
    rights = deepcut.tokenize(spliteQes[1]) if spliteQes[1] != "" and len(spliteQes[1]) >= 2 else []
    return lefts+[qiesTag]+rights

def keyData(dataVacab,DATA_KEY,texts):
    get_input_labels
    topdata = []
    words = cuteringt(texts)
    ccss = []
    for x in pos_tag(words,engine='old'):
        if x[1] is None:
            ccss.append("None")
        else:
            ccss.append(x[1])
    keydata = list(DATA_KEY.keys())
    intWord = get_input_labels(dataVacab,words)
    mainData = [x for x in keydata if x in intWord] 
    allindex = [n+1 for n, i in enumerate(intWord) if i == mainData[0]]  
    left = DATA_KEY[mainData[0]]["left"]
    right = DATA_KEY[mainData[0]]["right"]
    dataset = DATA_KEY[mainData[0]]["data"]
    indexs = allindex[-1]
    if(left != 0):
        lenadd = indexs
        for x in range(lenadd):
            cptext = int(str(2)+"".join(map(str,intWord[x:indexs])) )
            if(cptext in dataset):
                keyIdex = lenadd - len(intWord[x:indexs]) - 1
                if(keyIdex > 0 and words[keyIdex] != '<space>'):
                    topdata.append([2,words[keyIdex],len(intWord[x:indexs])])      
    if(right != 0):
        lenadd = len(intWord) -  indexs + 1
        for x in range(lenadd):
            cptext = int(str(1)+"".join(map(str,intWord[indexs-1:x+indexs])) )
            if(cptext in dataset): 
                keyIdex = len(intWord[indexs-1:x+indexs]) + 2
                try:
                    if(keyIdex > 0 and words[keyIdex] != '<space>'):
                        topdata.append([1,words[keyIdex],len(intWord[indexs:x+indexs])])
                except:
                    return [],[]
    return words,topdata
        
    

            
    
if __name__ == "__main__":
    #1.create data dictionnary 
    pathData = "dataRaw\\QuestionGen.txt"
    createVocabulary(pathData,"keywordQ",loadData=False)


    #load vocab data
    dataVacabs = load_obj("keywordQ")
    wordQuestion,wordKeyword,DATA_KEY,errorQ,errorSentene = prepareData("keywordGenerate",pathData,dataVacabs,loadData=False)
	
    '''
    DATA_KEY = load_obj("keywordGenerate")
    print(keyData(dataVacabs,DATA_KEY,texts))
    '''
