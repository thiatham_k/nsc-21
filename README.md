## โฟล์เดอร์ทั้งหมด
> dataRaw คือ โฟล์เดอร์เก็บไฟล์ raw data ที่ยังไม่มีการทำอะไร

> dataPre คือ โฟล์เดอร์เก็บไฟล์ data ที่มกีการ analysis เเล้ว

## ไฟล์ไพทอนทั้งหมด 
> keywordGeneration.py

- def createVocabulary หน้าที่เก็บคำไว้ใน dict เพื่อเก็บคำเป็น id
```bash
    pathData = "dataRaw\\QuestionGen.txt"
    #save to keywordQ.pkl
    createVocabulary(pathData,"keywordQ",loadData=False) 
```
- def prepareData หน้าที่คือการสร้างไฟล์เตรียมสำหรับทำการใช้หา keyword
```bash
    #load to keywordQ.pkl
    dataVacabs = load_obj("keywordQ")
    wordQuestion,wordKeyword,DATA_KEY,errorQ,errorSentene = prepareData("keywordGenerate",pathData,dataVacabs,loadData=False)
```
- def keyData หน้าที่รับ input Question มาเเละหา keyword
```bash
    #load keywordGennerate.plk
    DATA_KEY = load_obj("keywordGenerate")
    texts = "เเม่ใช้ซื้ออะไร"
    #print [num_term,str_keyword,จำไม่ได้]
    print(keyData(dataVacabs,DATA_KEY,texts))
```
> test.py
- def spliteQes  ทำการเตรียมดาต้าเบสสำหรับใช้ต่อไปในรูปเเบบ json เพื่อความเร็ว
```bash
    nameJson = "QandA.json"
    spliteQes(nameJson,"indexdir","อะไร"):
```

- def scoreTest  ทำการตรวจสอบการใช้ keyword เพื่อหาช่วงคำตอบ
```bash
    nameJson = "QandA.json"
    scoreTest(nameJson,"indexdir","อะไร")
```
- def Tester ทำการบันทึก keyword กับคำถามทั้งหมดไปไว้ในไฟล์ saveTest.txt
```bash
    nameJson = "QandA.json"
    Tester(nameJson,"indexdir","อะไร")
```
## License
[MIT](https://choosealicense.com/licenses/mit/)